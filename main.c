#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define LINE_LEN 20

void printBubble(const char * message);
void printCow();
char * joinMessage(int argc, const char ** argv);
size_t findWidth(const char * message);

int main(int argc, const char ** argv)
{
    char * message = joinMessage(argc-1, argv+1);

    printBubble(message);
    printCow();

    free(message);

    return EXIT_SUCCESS;
}

void printBubble(const char * message)
{
    size_t width = findWidth(message) + 2;

    printf(" ");
    for (int i = 0; i < width; i++)
    {
        printf("_");
    }
    printf("\n< %s >\n ", message);
    for (int i = 0; i < width; i++)
    {
        printf("-");
    }
}

void printCow()
{

}

/*
 * Allocates and returns a joined string constructed from the contents of argv
 *
 * @free  The returned string
 *
 * @param argc  The number of elements to be joined
 * @param argv  The elements to be joined
 * @returns  An allocated string of the joined elements, separated by spaces
 */
char * joinMessage(int argc, const char ** argv)
{
    size_t messageLength = 0;
    // Add up the lengths of argv[1]...argv[n]
    for (int i = 1; i < argc; i++)
    {
        messageLength += strlen(argv[i]);
    }
    // Add 1 for terminating NULL
    char * message = malloc(messageLength + 1);

    for (int i = 0, index = 0; i < argc; index += strlen(argv[i]) + 1, i++)
    {
        if (i > 0)
        {
            // Add a space between each word
            message[index - 1] = ' ';
        }
        strcpy(message + index, argv[i]);
    }
    return message;
}

size_t findWidth(const char * message)
{
    size_t width = 0;
    for (size_t i = 0; i < strlen(message); i++)
    {
        if (message[i] == '\n' && i > width)
        {
            width = i;
        }
    }
    // If there are no newlines
    if (width == 0)
    {
        width = strlen(message);
    }
    return width;
}
